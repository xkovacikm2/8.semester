package p2;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Test pre multilanguage support - ResourceBundle
 * @author Jaroslav Jakubik
 */
public class RTest {

	private static final String TEXTIKY = "textiky";
	
	public static void main(String[] args) {
		ResourceBundle rb = ResourceBundle.getBundle(TEXTIKY, Locale.forLanguageTag("sk"));
		
		String t = rb.getString("text1");
		System.out.println(t);
	}

}
