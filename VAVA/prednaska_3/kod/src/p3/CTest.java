package p3;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test logera a konfiguracie prostrednictvom properties
 * @author Jaroslav Jakubik
 */
public class CTest {

	private static Logger LOG = Logger.getLogger("p3.CTest");
	
	public static void main(String[] args) throws Exception {
		LOG.info("1 - start");
		Properties p = new Properties();
		
		LOG.severe("2 - mam vytvorene properties");
		p.load(new FileInputStream("etc/conf.properties"));
		
		LOG.warning("3 - skoro hotovo");
		LOG.finest("3 - skoro hotovo");
		
		LOG.info((String)p.get("spustitSimulaciu"));
		
		try {
			int a = 10/0;
		} catch(Exception e) {
			LOG.log(Level.SEVERE, "Pozor chyba", e);
		}
	}

}
