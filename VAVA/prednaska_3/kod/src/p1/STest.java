package p1;

import java.util.Random;

/**
 * Jednoduchy test pre porovnanie +, StringBuilder-a a StringBuffer
 * @author Jaroslav Jakubik
 */
public class STest {

	private static final int MAX = 50000;
	private static Random R = new Random();
	
	public static void main(String[] args) {
		test1();
		test2();
		test3();
	}

	/**
	 * vygenerovanie retazca cez operaciu +
	 * @return
	 */
	private static String test1() {
		long start = System.currentTimeMillis();
		String result = "";
		for(int i=0; i<MAX; i++) {
			result += generateStr();
		}
		long end = System.currentTimeMillis();
		System.out.println("+ " + (end - start));
		return result;
	}

	/**
	 * vygenerovanie retazca cez operaciu StringBuilder
	 * @return
	 */
	private static String test2() {
		long start = System.currentTimeMillis();
		StringBuilder result = new StringBuilder();
		for(int i=0; i<MAX; i++) {
			result.append(generateStr());
		}
		long end = System.currentTimeMillis();
		System.out.println("StringBuilder " + (end - start));
		return result.toString();
	}

	/**
	 * vygenerovanie retazca cez operaciu StringBuffer
	 * @return
	 */
	private static String test3() {
		long start = System.currentTimeMillis();
		StringBuffer result = new StringBuffer();
		for(int i=0; i<MAX; i++) {
			result.append(generateStr());
		}
		long end = System.currentTimeMillis();
		System.out.println("StringBuffer " + (end - start));
		return result.toString();
	}

	/**
	 * jednoduche generovatko nahodneho textu
	 * @return
	 */
	private static String generateStr() {
		//return String.valueOf(R.nextLong());
		return "1";
	}
}
