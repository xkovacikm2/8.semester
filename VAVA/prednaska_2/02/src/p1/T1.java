package p1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Testovacia trieda pre:
 * - porovnanie performance kolekcii
 * - ukazku ordering-u
 * 
 * @author Jaroslav Jakubik
 */
public class T1 {

	public static final int ITEM_NUM = 1000;
	public static final int READ_ITEM_NUM = 1000;
	
	public static void main(String[] args) {
		List<Integer> l1 = new ArrayList<Integer>(ITEM_NUM);
		fillList(l1);
		readList(l1);
		
		List<Integer> l2 = new LinkedList<Integer>();
		fillList(l2);
		readList(l2);
		
		printList(l1);
		
		//usporiadanie podla natural ordering
		Collections.sort(l1);
		printList(l1);

		//usporiadanie s custom comparator
		Collections.sort(l1, (Integer arg0, Integer arg1) -> {
				if(arg0 > arg1) {
					return -1;
				} else if(arg0 == arg1) {
					return 0;
				} else {
					return 1;	
				}
			});
		
		printList(l1);
	}

	/**
	 * Naplnenie zoznamu nahodne vygenerovanymi cislami
	 * @param lst
	 */
	public static void fillList(List<Integer> lst) {
		Long start = System.currentTimeMillis();
		Random r = new Random();
		for(int i = 0; i<ITEM_NUM; i++) {
			lst.add(r.nextInt(ITEM_NUM));
		}
		Long end = System.currentTimeMillis();
		System.out.println("Fill " + (end - start));
	}
	
	/**
	 * Citanie nahodnych prvkov zo zoznamu
	 * @param lst
	 */
	public static void readList(List<Integer> lst) {
		Long start = System.nanoTime();
		Random r = new Random();
		int lstSize = lst.size();
		for(int i = 0; i<READ_ITEM_NUM; i++) {
			lst.get(r.nextInt(lstSize));
		}
		Long end = System.nanoTime();
		System.out.println("Read " + (end - start));		
	}
	
	/**
	 * Vypis prvkov zoznamu do riadku oddelene ciarkou
	 * @param lst
	 */
	public static void printList(List<Integer> lst) {
		for(Integer i : lst) {
			System.out.print(i + ", ");
		}
		System.out.println();
	}
}
