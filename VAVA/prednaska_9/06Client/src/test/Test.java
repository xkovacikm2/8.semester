package test;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Jednoducha trieda pre otestovanie putnutia spravy do queue
 * @author Jaroslav Jakubik
 */
public class Test {

	public static void main(String[] args) throws Exception {
		//pouzije sa context na zaklade konfiguracie z jndi.properties
		Context ctx = new InitialContext();
		QueueConnectionFactory qcf = (QueueConnectionFactory)ctx.lookup("jms/RemoteConnectionFactory");
		QueueConnection conn = qcf.createQueueConnection("testUser", "testUser");
		
		QueueSession session = conn.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
		
		conn.start();
		
		Queue queue = (Queue)ctx.lookup("jms/queue/test");
		QueueSender sender = session.createSender(queue);
		
		Message msg = session.createTextMessage("Zaloguj please main ... ");
		sender.send(msg);
		
		session.close();
		conn.stop();
		conn.close();
	}

}
