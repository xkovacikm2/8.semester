package entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * Entity class-a pre jedno zrebovania (v ramci dna)
 * @author Jaroslav Jakubik
 */
@Entity
@NamedQuery(
		name="entity.Lotery.getByDate", 
		query="select lo from Lotery lo where lo.loteryDate < :lotDate"
)
public class Lotery {

	@Id
	private Long id;
	private Date loteryDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLoteryDate() {
		return loteryDate;
	}

	public void setLoteryDate(Date loteryDate) {
		this.loteryDate = loteryDate;
	}
	
}
