
package wsClient.stub;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the wsClient.stub package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DoPlus_QNAME = new QName("http://test/", "doPlus");
    private final static QName _DoPlusResponse_QNAME = new QName("http://test/", "doPlusResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: wsClient.stub
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DoPlus }
     * 
     */
    public DoPlus createDoPlus() {
        return new DoPlus();
    }

    /**
     * Create an instance of {@link DoPlusResponse }
     * 
     */
    public DoPlusResponse createDoPlusResponse() {
        return new DoPlusResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPlus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://test/", name = "doPlus")
    public JAXBElement<DoPlus> createDoPlus(DoPlus value) {
        return new JAXBElement<DoPlus>(_DoPlus_QNAME, DoPlus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPlusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://test/", name = "doPlusResponse")
    public JAXBElement<DoPlusResponse> createDoPlusResponse(DoPlusResponse value) {
        return new JAXBElement<DoPlusResponse>(_DoPlusResponse_QNAME, DoPlusResponse.class, null, value);
    }

}
