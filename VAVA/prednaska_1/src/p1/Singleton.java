package p1;

public class Singleton {
  public static Singleton INSTANCE;

  private Singleton() {}

  public static Singleton getInstance(){
    if (INSTANCE == null) {
      INSTANCE = new Singleton();
    }
    return INSTANCE;
  }

  public void doSomething(){}
}
