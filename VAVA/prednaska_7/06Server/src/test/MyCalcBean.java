package test;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entity.Lotery;
import entity.WinNumbers;

/**
 * Stateless session beana pre otestovanie entity managera
 * @author Jaroslav Jakubik
 */
@Stateless
@LocalBean
public class MyCalcBean {

	@PersistenceContext
	private EntityManager manager;
	
	public Integer doPlus(Integer i1, Integer i2) {
		tryEntityManager1();
		tryEntityManager2();
		
		return i1 + i2;
	}

	/**
	 * Jednoduchy test pouzitia entity managera
	 * - load entity Lotery na zaklade ID
	 * - vytvorenie entity WinNumbers
	 * - persistovanie entity WinNumbers
	 */
	private void tryEntityManager1() {
		Lotery l = manager.find(Lotery.class, 1l);
		
		WinNumbers wn = new WinNumbers();
		wn.setId(6l);
		wn.setLotery(l);
		wn.setNum1(1);
		wn.setNum1(11);
		wn.setNum1(21);
		wn.setNum1(31);
		wn.setNum1(41);
		wn.setNum1(51);
		
		manager.persist(wn);
		
		System.out.println(l);
	}
	
	/**
	 * Jednoduchy test pouzitia entity managera
	 * - named query
	 * - native query
	 */
	private void tryEntityManager2() {
		Query q = manager.createNamedQuery("entity.Lotery.getByDate");
		q.setParameter("lotDate", new Date());
		List<Lotery> l1 = (List<Lotery>)q.getResultList();
		System.out.println(l1);
		
		Query q2 = manager.createNativeQuery("select 1 from dual");
		Object obj = q2.getSingleResult();
		System.out.println(obj);		
	}
}
