package test;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Jednoducha stateless session beana
 */
@Stateless
public class TestBean implements TestBeanRemote {

	@EJB
	private MyCalcBean calc;
	
	/**
	 * Biznis logika ktora vrati rozsireny textovy retazec
	 */
    public String testMe(String input) {
    	System.out.println(calc.doPlus(1, 3));    	
    	return "tested " + input;
    }

}
