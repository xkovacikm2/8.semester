package entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity class-a pre jeden tah cisel
 * @author Jaroslav Jakubik
 */
@Entity
public class WinNumbers {

	@Id
	private Long id;
	
	private Integer num1;
	
	private Integer num2;
	
	private Integer num3;
	
	private Integer num4;
	
	private Integer num5;
	
	private Integer num6;
	
	@ManyToOne
	private Lotery lotery;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNum1() {
		return num1;
	}

	public void setNum1(Integer num1) {
		this.num1 = num1;
	}

	public Integer getNum2() {
		return num2;
	}

	public void setNum2(Integer num2) {
		this.num2 = num2;
	}

	public Integer getNum3() {
		return num3;
	}

	public void setNum3(Integer num3) {
		this.num3 = num3;
	}

	public Integer getNum4() {
		return num4;
	}

	public void setNum4(Integer num4) {
		this.num4 = num4;
	}

	public Integer getNum5() {
		return num5;
	}

	public void setNum5(Integer num5) {
		this.num5 = num5;
	}

	public Integer getNum6() {
		return num6;
	}

	public void setNum6(Integer num6) {
		this.num6 = num6;
	}

	public Lotery getLotery() {
		return lotery;
	}

	public void setLotery(Lotery lotery) {
		this.lotery = lotery;
	}

}
