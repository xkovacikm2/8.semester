package test;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import entity.Lotery;
import entity.WinNumbers;

/**
 * Testovanie zakladny funkcii objektovo relacneho mapovania
 * - pripojenie k DB
 * - insert zaznamov
 * - select na zaklade IDcka
 * - select na zaklade query
 * 
 * @author Jaroslav Jakubik
 */
public class Runner {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();

		Lotery l = new Lotery();
		l.setId(1l);
		l.setLoteryDate(new Date());
		
		session.persist(l);
		
		WinNumbers wn = new WinNumbers();
		wn.setNum1(11);
		wn.setNum2(8);
		wn.setNum3(65);
		wn.setNum4(16);
		wn.setNum5(23);
		wn.setNum6(8);
		wn.setLotery(l);
		wn.setId(2l);
		
		session.persist(wn);
		
		Lotery l1 = (Lotery)session.load(Lotery.class, 1l);
		System.out.println(l1.getId() + " - " + l1.getLoteryDate());
		
		Query q = session.createQuery("select num.num2, num.num3 from WinNumbers as num join num.lotery where num.id = 2");
		List<WinNumbers> result = q.list();
		
		System.out.println("Vysledok " + result.size());
		
		transaction.commit();
	}

}
