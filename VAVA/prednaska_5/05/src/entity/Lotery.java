package entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Entity class-a pre jedno zrebovania (v ramci dna)
 * @author Jaroslav Jakubik
 */
@Entity
public class Lotery {

	@Id
	private Long id;
	private Date loteryDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLoteryDate() {
		return loteryDate;
	}

	public void setLoteryDate(Date loteryDate) {
		this.loteryDate = loteryDate;
	}
	
}
