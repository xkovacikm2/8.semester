package test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import pojo.Author;
import pojo.Book;
import pojo.Library;

/**
 * Test classa pre serializaciu POJO objektov do XML
 * @author Jaroslav Jakubik
 */
public class FromObject2Xml {

	public static void main(String[] args) throws Exception {
		Library dataForSerialization = prepareTestData();
		File file = new File("output.xml");
		
		JAXBContext context = JAXBContext.newInstance(Library.class);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(dataForSerialization, file);
		//marshaller.marshal(dataForSerialization, System.out);
	}

	/**
	 * vygeneruje testovaciu strukturu objektov, ktoru chceme serializovat
	 * @return
	 */
	private static Library prepareTestData() {
		Library l = new Library();
		l.setAddress("Adresa nasej kniznice, Bratislava 1");
		
		List<Book> books = new ArrayList<Book>();
		
		Book b1 = new Book();
		b1.setPageNum(150l);
		
		Author a1 = new Author();
		a1.setName("Ferdo Mravec Ml.");
		a1.setAddress("Mravenisko 5, Bratislava 2");

		b1.setAuthor(a1);
		books.add(b1);
		
		Book b2 = new Book();
		b2.setPageNum(345l);
		
		Author a2 = new Author();
		a2.setName("Vilko Vcielka");
		a2.setAddress("Ul 14, Bratislava 5");

		b2.setAuthor(a2);
		books.add(b2);
		
		l.setBooks(books);
		
		return l;
	}
}
