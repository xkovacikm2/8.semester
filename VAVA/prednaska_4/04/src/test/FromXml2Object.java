package test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import pojo.Library;

/**
 * Vytvorenie stromu objektov z XML, vyuzitie JAXB deserializacie pre precitanie XML
 * @author Jaroslav Jakubik
 */
public class FromXml2Object {

	public static void main(String[] args) throws Exception {
		File file = new File("output.xml");
		JAXBContext context = JAXBContext.newInstance(Library.class);

		Unmarshaller unmarshaller = context.createUnmarshaller();
		Library library = (Library) unmarshaller.unmarshal(file);
		
		System.out.println(library.getAddress() + ", pocet knih: " + library.getBooks().size());
	}

}
