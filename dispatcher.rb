require 'sinatra'
require 'json'
require 'rest-client'

set :environment, :production
set :bind, 4567

post "/image_upload" do
  puts request.env['rack.request.form_hash']
  halt 400, 'wrong parameters expecting file and user_id' if params[:file].nil? or params[:user_id].nil?
  filepath= '/var/www/' + params[:file][:filename]
  File.open(filepath, 'w') do |f|
    f.write params[:file][:tempfile].read
  end
  fork {handle_image filepath, params[:user_id]}
  {message: 'file was uploaded', channel: "/#{params[:user_id]}"}.to_json
end

def handle_image(filepath, user_id)
  #send file for OCR
  ocr_result = RestClient.post 'http://147.175.181.159:4000/file', {'filename' => filepath}.to_json, {content_type: :json}
  #test sending XML to WS server
  RestClient.post 'http://147.175.181.159:9292/', {'channel' => user_id, 'message' => ocr_result.body}
end

