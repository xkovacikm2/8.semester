#! /bin/bash
cd /opt/openshift-origin-v1.4.1
##openshift start master --config /etc/openshift/master/master-config.yaml
export OPENSHIFT=/opt/openshift-origin-v1.4.1
export OPENSHIFT_VERSION=v1.4.1
export PATH=$OPENSHIFT:$PATH
export KUBECONFIG=$OPENSHIFT/openshift.local.config/master/admin.kubeconfig
export CURL_CA_BUNDLE=$OPENSHIFT/openshift.local.config/master/ca.crt
./openshift start master --config /opt/openshift-origin-v1.4.1/openshift.local.config/master/master-config.yaml
